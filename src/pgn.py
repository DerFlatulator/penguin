#!/usr/bin/python

import os, sys, traceback
from os.path import expanduser
from cmd import Cmd

# Find the width of the running Console
def cwidth(): return int(os.popen('stty size', 'r').read().split()[1])

VERSION = '0.5'
penguin = [ \
   "a8888b." \
  ,"d888888b." \
  ,"8P\"YP\"Y88" \
  ,"8|o||o|88" \
  ,"8'    .88" \
  ,"8`._.' Y8." \
  ,"d/      `8b." \
  ,"dP   .    Y8b." \
  ,"d8:'  \"  `::88b" \
  ,"d8\"         'Y88b" \
  ,":8P    '      :888 " \
  ,"8a.   :     _a88P" \
  ,"._/\"Yaa_:   .| 88P|  " \
  ,"\\    YP\"    `| 8P  `." \
  ,"/     \\.___.d|    .' " \
  ,"`--..__)8888P`._.'   " ]

def printpenguin():
  """Print the intro banner"""
  cntr = '{:=^' + str(cwidth()) + '}'
  cntp = '{:^' + str(cwidth()) + '}'
  print cntr.format(' Penguin Shell ')
  for line in penguin: print cntp.format(line)
  print cntr.format(' Version %s ' % VERSION)
  
printpenguin()

def color(string, col):
  """Make a <string> print <col>."""
  colors = { 'end': '\033[0m',   'blue': '\033[94m', 'green': '\033[92m', \
          'warn': '\033[93m', 'fail': '\033[91m' }
  return ''.join([ colors.get(col, ""), string, colors['end']])

#def replace(list, condition, replaceVal):
#  return [(replaceVal(e) if condition(list) else e) for e in list]
   
class Penguin(Cmd):
  """Subclass of Cmd, implements command prompt"""

  def __init__(self):
    Cmd.__init__(self)
    self.done = False

  def setprompt(self): 
    """Sets the prompt to be printed before each command""" 
    self.wd = os.getcwd()
    homedir = expanduser("~")
    if self.wd.startswith(homedir):
      self.wd = self.wd.replace(homedir, '~', 1)
        
    self.prompt = color('(', 'blue') + color(self.wd, 'green') + \
      color(')> ', 'blue') 
  
  def preloop(self):
    self.setprompt()

  def precmd(self, line):
    # FIXME: Add actual logic for quotations
    return line.replace('"', "").replace("'", "")

  def postcmd(self, stop, line):
    self.setprompt()
    return self.done

  def emptyline(self):
    print color('No command entered. Enter ? or help for information.', 'warn')

  def default(self, line):
    print color("Command not found: " + line.split(' ')[0], 'fail')
    #print '%sCommand not found: %s%s' % \
    #  (color['fail'], line.split(' ')[0], color['end'])
  
  def do_penguin(self, line):
    """Prints the intro banner and penguin"""
    printpenguin()

  def do_cd(self, line):
    """Changes directory.
    cd <directory>\t- Change to <directory>
    cd            \t- Print the current directory"""
    # Print working directory if no argument is given
    if len(line) == 0:
      print self.wd
    else: 
      # Attempt to CD into directory
      try:
        os.chdir(expanduser(line))
      except OSError as e:
        print color({ \
          13: "Permission Denied", 2: "Directory Not Found", 20: "Not a Directory" \
        }.get(e.errno, "Unknown Error"), 'fail') 
        
      finally:
        self.setprompt()
 
  def do_clr(self, line):
    """Clear the screen"""
    os.system("clear")
 
  def do_dir(self, line):
    """Lists the contents of the current directory
    dir    \t- List contents of the current directory
    dir -a \t- Include hidden files
    dir -1 \t- Print one column only"""
   
    args = line.split(' ')
    showhidden = '-a' in args  or '--all' in args
    files = sorted(os.listdir(os.getcwd()))
    if not showhidden: # Filter out files not starting with '.'
      files = filter (lambda f: not f.startswith('.'), files)

    # Make directories green with a trailing slash      
    #files = [(color(f, 'green') + '/' if os.path. isdir(f) else f) for f in files]
    #files = [(color(f, 'blue') +  '/' if os.path.islink(f) else f) for f in files]
    files = [make_pretty(f) for f in files]
    # files = replace(files, lambda f: os.path.isdir(f), lambda f: color(f, 'green'))
    
    filestr = ""
    if '-1' in args:
      cols = 1
    else:
      # Dynamically adjust columns to the width of the console
      longestfile = len(max(files, key=len))
      cols = cwidth() // (longestfile + 4) 
    
    w = cwidth() // cols 
    # if w < longestfile: w = cwidth()
    nl = 0
    for f in files:
      nl += 1
      if f.endswith('/'): # Extra 9 characters are invisible
        filestr += ('{:<' + str(w+9) + '}').format(f)
      else:
        filestr += ('{:<' + str(w) + '}').format(f)
      
      if nl == cols or w == cwidth(): 
        filestr += '\n'
        nl = 0
      
    print filestr

  def do_environ(self, line):
    """Lists all thed environment variables and their values"""
    for var, val in os.environ.iteritems():
      print '='.join([color(var, 'green'), color(val, 'blue')])
  
  def do_echo(self, line):
    """Print to the screen
    echo <comment>\t- Print <comment>"""
    print line

  def do_help(self, line):
    """Provdes usage information"""
    Cmd.do_help(self, line) 
  
  def do_pause(self, line):
    """Pause the shell until <Enter> is pressed"""
    __import__("getpass").getpass(prompt="Waiting for <Enter>...")
  
  def do_quit(self, line):
    """Exits the penguin shell"""
    self.done = True  
      
  def complete_cd(self, text, line, start, end):
    #print "\nAutocompletion is Work in Progress, expect bugs"
    return ls_complete(line, end)

#end class

def extract_arg(line, end): return line[line.rfind(" ", 0, end):end]    

def ls_complete(line, cursor):
  head, tail = os.path.split(extract_arg(line, cursor).strip())
  if head == "": head = "."
  return [ f + "/" for f in filter(lambda d: os.path.isdir(d) or os.path.islink(d), \
                                   os.listdir(head)) if f.startswith(tail) ]
 
def make_pretty(path): 
    if os.path.isdir(path):  return color(path, 'green') + '/' 
    if os.path.islink(path): return color(path, 'blue') + '/' 
    return path        
      
def main():
  try:
    prompt = Penguin()
    prompt.cmdloop()
  except KeyboardInterrupt: # Disable Ctrl-C
    print "^C"
    main() 
  except Exception:
    traceback.print_exc(file=sys.stdout)

  sys.exit(0)

if __name__ == "__main__":
  main()
